# Assembleia

## Api para gerenciar pautas e votos

## Nesta branch foram adicionados Testes de integracao, utilizando testContainer e Wiremock para reproduzir o ambiente

### Endpoints
 - criar pauta           `localhost:8080/pauta`  POST
 - criar sessao de votos `localhost:8080/sessao` POST
 - votar                 `localhost:8080/voto`   POST
 - buscar pauta votada   `localhost:8080/voto`   GET

### 🛠 Tecnologias
  - java 17
  - gradle
  - docker
  - docker compose

### Dependencia
Este projeto integra com uma api com limite de uso que valida o CPF, ela necessita de um token, estou fornecendo um, mas caso necessario, um novo token pode ser gerado [bem aqui.](https://api.invertexto.com) 
O token deve ser 

### 🎲 Rodando a api

```bash
# Na raiz do projeto execute docker para subir o banco
$ docker compose up -d && ./gradlew build bootRun

# Aguarde o banco subir e em seguida build o projeto
#$ ./gradlew build bootRun


# O servidor inciará na porta:8080
```

### Debitos tenicos
- versionamento da api, optaria por versionar por path   ``pauta/v1``
- documentacao de endpoint, optaria por ``openapi``
- teste unitarios
- teste de integracao, optaria por mocar banco com [test-container](https://www.testcontainers.org/quickstart/junit_5_quickstart/), aqui nos subimos um container contendo o que precisamos mocar para o teste
- teste de integracao, usar [wiremock](https://wiremock.org/docs/stubbing/) para mocar chamadas a apis externas
- teste de carga, conheci e usei gatling, mas optaria por usar o [K6](https://k6.io/docs/test-types/load-testing/), esse carinha eh bem legal, foi feito em golang, mas possui a sintax simples do javascript.

### Collection 
```json
{
  "info": {
    "_postman_id": "f94b24f6-9111-47f7-ad52-d01b18763452",
    "name": "votacao",
    "schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json",
    "_exporter_id": "23585045"
  },
  "item": [
    {
      "name": "buscar pauta",
      "request": {
        "method": "GET",
        "header": [],
        "url": {
          "raw": "http://localhost:8080/pauta/1",
          "protocol": "http",
          "host": [
            "localhost"
          ],
          "port": "8080",
          "path": [
            "pauta",
            "1"
          ]
        }
      },
      "response": []
    },
    {
      "name": "criar pauta",
      "request": {
        "method": "POST",
        "header": [],
        "body": {
          "mode": "raw",
          "raw": "{\n    \"descricao\": \"trocar portaria\",\n    \"qtdMinVotos\": 2\n}",
          "options": {
            "raw": {
              "language": "json"
            }
          }
        },
        "url": {
          "raw": "http://localhost:8080/pauta",
          "protocol": "http",
          "host": [
            "localhost"
          ],
          "port": "8080",
          "path": [
            "pauta"
          ]
        }
      },
      "response": []
    },
    {
      "name": "criar sessao de voto",
      "request": {
        "method": "POST",
        "header": [],
        "body": {
          "mode": "raw",
          "raw": "{\n    \"idPauta\": 1,\n    \"qtyMinutosSessao\": 1\n}",
          "options": {
            "raw": {
              "language": "json"
            }
          }
        },
        "url": {
          "raw": "http://localhost:8080/sessao",
          "protocol": "http",
          "host": [
            "localhost"
          ],
          "port": "8080",
          "path": [
            "sessao"
          ]
        }
      },
      "response": []
    },
    {
      "name": "registrar voto",
      "request": {
        "method": "POST",
        "header": [],
        "body": {
          "mode": "raw",
          "raw": "{\n    \"sessaoId\": 1,\n    \"cpf\": \"00345256026\",\n    \"voto\": \"NAO\" \n}",
          "options": {
            "raw": {
              "language": "json"
            }
          }
        },
        "url": {
          "raw": "http://localhost:8080/voto",
          "protocol": "http",
          "host": [
            "localhost"
          ],
          "port": "8080",
          "path": [
            "voto"
          ]
        }
      },
      "response": []
    }
  ]
}
```
