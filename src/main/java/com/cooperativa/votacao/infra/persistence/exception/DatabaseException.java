package com.cooperativa.votacao.infra.persistence.exception;

public class DatabaseException extends RuntimeException {
    public DatabaseException(String message, Throwable error) {
        super(message, error);
    }
}
