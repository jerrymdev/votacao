package com.cooperativa.votacao.infra.persistence.repository;

import com.cooperativa.votacao.infra.persistence.entity.PautaEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface PautaRepository extends ReactiveCrudRepository<PautaEntity, Long> {
}
