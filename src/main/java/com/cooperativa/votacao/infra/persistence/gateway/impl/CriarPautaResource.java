package com.cooperativa.votacao.infra.persistence.gateway.impl;

import com.cooperativa.votacao.infra.persistence.entity.PautaEntity;
import com.cooperativa.votacao.infra.persistence.exception.DatabaseException;
import com.cooperativa.votacao.infra.persistence.gateway.CriarPautaGateway;
import com.cooperativa.votacao.infra.persistence.repository.PautaRepository;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class CriarPautaResource implements CriarPautaGateway {
    private final PautaRepository repository;

    public CriarPautaResource(PautaRepository repository) {
        this.repository = repository;
    }

    @Override
    public Mono<PautaEntity> execute(PautaEntity pautaEntity) {
        pautaEntity.setCreatedAt(LocalDateTime.now());
        return repository.save(pautaEntity)
                .doFirst(() -> log.info("salvando pauta={} na base de dados", pautaEntity))
                .doOnError(ex -> {
                    log.error("erro ao salvar pauta na base de dados", ex);
                    throw new DatabaseException("nao foi possivel criar pauta", ex);
                })
                .doOnSuccess(associado -> log.info("pauta={} salvo na base de dados", associado));
    }
}
