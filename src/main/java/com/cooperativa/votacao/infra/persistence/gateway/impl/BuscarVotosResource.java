package com.cooperativa.votacao.infra.persistence.gateway.impl;

import com.cooperativa.votacao.infra.persistence.entity.VotoEntity;
import com.cooperativa.votacao.infra.persistence.exception.DatabaseException;
import com.cooperativa.votacao.infra.persistence.gateway.BuscarVotosGateway;
import com.cooperativa.votacao.infra.persistence.gateway.RegistrarVotoGateway;
import com.cooperativa.votacao.infra.persistence.repository.VotoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class BuscarVotosResource implements BuscarVotosGateway {
    private final VotoRepository votoRepository;

    public BuscarVotosResource(VotoRepository votoRepository) {
        this.votoRepository = votoRepository;
    }

    @Override
    public Flux<VotoEntity> execute(Long sessaoId) {
        return votoRepository.findBySessaoId(sessaoId)
                .doFirst(() -> log.info("buscando votos da sessao={} na base de dados", sessaoId))
                .doOnError(ex -> {
                    log.error("erro ao buscar votos da sessao={}", sessaoId, ex);
                    throw new DatabaseException("nao foi possivel buscar votos", ex);
                });
//                .doOnSuccess(result -> log.info("voto={} registrado na base de dados", result));
    }
}
