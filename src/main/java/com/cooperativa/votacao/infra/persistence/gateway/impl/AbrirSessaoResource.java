package com.cooperativa.votacao.infra.persistence.gateway.impl;

import com.cooperativa.votacao.infra.persistence.entity.SessaoVotacaoEntity;
import com.cooperativa.votacao.infra.persistence.exception.DatabaseException;
import com.cooperativa.votacao.infra.persistence.gateway.AbrirSessaoGateway;
import com.cooperativa.votacao.infra.persistence.repository.PautaRepository;
import com.cooperativa.votacao.infra.persistence.repository.SessaoRepository;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class AbrirSessaoResource implements AbrirSessaoGateway {
    private final PautaRepository pautaRepository;
    private final SessaoRepository sessaoRepository;

    public AbrirSessaoResource(PautaRepository pautaRepository, SessaoRepository sessaoRepository) {
        this.pautaRepository = pautaRepository;
        this.sessaoRepository = sessaoRepository;
    }

    @Override
    public Mono<SessaoVotacaoEntity> execute(Long pautaId, Integer tempoDeSessao) {
        return pautaRepository.existsById(pautaId)
                .doFirst(() -> log.info("verificando se pauta com id={} existe", pautaId))
                .flatMap(exists -> {
                    if (exists) {
                        return sessaoRepository.save(buildSessao(pautaId, tempoDeSessao));
                    } else {
                        log.error("pauta={} nao encontrada", pautaId);
                        return Mono.error(new Exception("pauta nao existe"));
                    }
                })
                .doOnError(ex -> {
                    throw new DatabaseException("erro ao criar sessao", ex);
                })
                .doOnSuccess(sessao -> log.info("sessao com a pauta={} aberta", pautaId));

    }

    private SessaoVotacaoEntity buildSessao(Long pautaId, Integer tempoDeSessao) {
        return SessaoVotacaoEntity.builder()
                .ativa(true)
                .createdAt(LocalDateTime.now())
                .pautaId(pautaId)
                .qtMinutos(tempoDeSessao)
                .build();
    }
}
