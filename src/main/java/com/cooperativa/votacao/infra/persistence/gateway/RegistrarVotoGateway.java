package com.cooperativa.votacao.infra.persistence.gateway;


import com.cooperativa.votacao.infra.persistence.entity.VotoEntity;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface RegistrarVotoGateway {
    Mono<VotoEntity> execute(VotoEntity voto);
}
