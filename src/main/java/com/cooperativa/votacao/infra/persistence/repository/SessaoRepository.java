package com.cooperativa.votacao.infra.persistence.repository;

import com.cooperativa.votacao.infra.persistence.entity.SessaoVotacaoEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface SessaoRepository extends ReactiveCrudRepository<SessaoVotacaoEntity, Long> {
}
