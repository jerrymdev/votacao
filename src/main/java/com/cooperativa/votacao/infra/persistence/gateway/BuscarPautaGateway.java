package com.cooperativa.votacao.infra.persistence.gateway;


import com.cooperativa.votacao.infra.persistence.entity.PautaEntity;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface BuscarPautaGateway {
    Mono<PautaEntity> execute(Long pautaId);
}
