package com.cooperativa.votacao.infra.persistence.gateway.impl;

import com.cooperativa.votacao.infra.persistence.entity.PautaEntity;
import com.cooperativa.votacao.infra.persistence.exception.DatabaseException;
import com.cooperativa.votacao.infra.persistence.gateway.BuscarPautaGateway;
import com.cooperativa.votacao.infra.persistence.repository.PautaRepository;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class BuscarPautaResource implements BuscarPautaGateway {
    private final PautaRepository repository;

    public BuscarPautaResource(PautaRepository repository) {
        this.repository = repository;
    }

    @Override
    public Mono<PautaEntity> execute(Long pautaId) {
        return repository.findById(pautaId)
                .doFirst(() -> log.info("buscando pauta={} na base de dados", pautaId))
                .switchIfEmpty(Mono.error(new RuntimeException("pauta nao existe na base de dados")))
                .doOnError(ex -> {
                    log.error("erro ao buscar pauta na base de dados");
                    throw new DatabaseException("nao foi possivel buscar pauta", ex);
                })
                .doOnSuccess(pauta -> log.info("pauta={} encontrado na base de dados", pauta));
    }
}
