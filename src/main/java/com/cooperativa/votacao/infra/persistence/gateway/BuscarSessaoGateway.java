package com.cooperativa.votacao.infra.persistence.gateway;


import com.cooperativa.votacao.infra.persistence.entity.SessaoVotacaoEntity;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface BuscarSessaoGateway {
    Mono<SessaoVotacaoEntity> execute(Long sessaoId);
}
