package com.cooperativa.votacao.infra.persistence.entity;

import java.time.LocalDateTime;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table("sessao_votacao")
public class SessaoVotacaoEntity {
    @Id
    public Long id;

    @Column("fk_pauta")
    public Long pautaId;

    public LocalDateTime createdAt;

    public Integer qtMinutos;

    public Boolean ativa;

}