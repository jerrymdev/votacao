package com.cooperativa.votacao.infra.persistence.gateway.impl;

import com.cooperativa.votacao.infra.persistence.entity.SessaoVotacaoEntity;
import com.cooperativa.votacao.infra.persistence.exception.DatabaseException;
import com.cooperativa.votacao.infra.persistence.gateway.BuscarSessaoGateway;
import com.cooperativa.votacao.infra.persistence.repository.SessaoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class BuscarSessaoResource implements BuscarSessaoGateway {
    private final SessaoRepository sessaoRepository;

    public BuscarSessaoResource(SessaoRepository sessaoRepository) {
        this.sessaoRepository = sessaoRepository;
    }

    @Override
    public Mono<SessaoVotacaoEntity> execute(Long sessaoId) {
        return sessaoRepository.findById(sessaoId)
                .doFirst(() -> log.info("buscando sessao com id={}", sessaoId))
                .switchIfEmpty(Mono.error(new RuntimeException("sessao nao existe")))
                .doOnError(ex -> {
                    log.error("erro ao buscar sessao={} na base de dados",sessaoId, ex);
                    throw new DatabaseException("nao foi possivel obter a sessao", ex);
                })
                .doOnSuccess(sessao -> log.info("sessao={} encontrada na base de dados", sessao));

    }
}
