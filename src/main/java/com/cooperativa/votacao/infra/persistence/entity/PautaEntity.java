package com.cooperativa.votacao.infra.persistence.entity;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table("pauta")
public class PautaEntity {
    @Id
    public Long id;

    public String descricao;

    public LocalDateTime createdAt;

    public Long qtdMinVotos;

    public Boolean aprovada;

    @Transient
    public SessaoVotacaoEntity sessaoVotacao;
}