package com.cooperativa.votacao.infra.persistence.gateway;


import com.cooperativa.votacao.infra.persistence.entity.VotoEntity;
import reactor.core.publisher.Flux;

@FunctionalInterface
public interface BuscarVotosGateway {
    Flux<VotoEntity> execute(Long sessaoId);
}
