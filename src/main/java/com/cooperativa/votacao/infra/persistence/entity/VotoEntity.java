package com.cooperativa.votacao.infra.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table("votos")
public class VotoEntity {
    @Id
    public Long id;

    public Long sessaoId;

    public String cpf;

    public Voto voto;

    public enum Voto {
            SIM("SIM"), NAO("NAO");

            private final String voto;

        Voto(String voto) {
            this.voto = voto;
        }
    }
}