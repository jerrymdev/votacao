package com.cooperativa.votacao.infra.persistence.repository;

import com.cooperativa.votacao.infra.persistence.entity.VotoEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface VotoRepository extends ReactiveCrudRepository<VotoEntity, Long> {
    Flux<VotoEntity> findBySessaoId(Long sessaoInd);
}
