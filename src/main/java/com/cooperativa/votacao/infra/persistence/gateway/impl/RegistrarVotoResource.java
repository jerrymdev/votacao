package com.cooperativa.votacao.infra.persistence.gateway.impl;

import com.cooperativa.votacao.infra.persistence.entity.VotoEntity;
import com.cooperativa.votacao.infra.persistence.exception.DatabaseException;
import com.cooperativa.votacao.infra.persistence.gateway.RegistrarVotoGateway;
import com.cooperativa.votacao.infra.persistence.repository.VotoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class RegistrarVotoResource implements RegistrarVotoGateway {
    private final VotoRepository votoRepository;

    public RegistrarVotoResource(VotoRepository votoRepository) {
        this.votoRepository = votoRepository;
    }

    @Override
    public Mono<VotoEntity> execute(VotoEntity voto) {
        return votoRepository.save(voto)
                .doFirst(() -> log.info("registrando voto={} na base de dados", voto))
                .doOnError(ex -> {
                    log.error("erro ao registrar voto={}", voto, ex);
                    throw new DatabaseException("nao foi possivel registrar voto", ex);
                })
                .doOnSuccess(result -> log.info("voto={} registrado na base de dados", result));
    }

}
