package com.cooperativa.votacao.infra.integration.cpfvalidate.gateway;

import com.cooperativa.votacao.infra.integration.cpfvalidate.response.CpfValidateResponse;
import reactor.core.publisher.Mono;

public interface ValidarCPFGateway {
    Mono<CpfValidateResponse> execute(String cpf);
}
