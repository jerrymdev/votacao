package com.cooperativa.votacao.infra.integration.cpfvalidate.gateway.impl;

import com.cooperativa.votacao.infra.integration.cpfvalidate.exception.ClientException;
import com.cooperativa.votacao.infra.integration.cpfvalidate.gateway.ValidarCPFGateway;
import com.cooperativa.votacao.infra.integration.cpfvalidate.response.CpfValidateResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class ValidarCPFResource implements ValidarCPFGateway {
    @Value("${api-parceira.cpf-validator.token}")
    private String token;

    private final WebClient webclient;

    public ValidarCPFResource( WebClient clientCpfValidator) {
        this.webclient = clientCpfValidator;
    }

    @Override
    public Mono<CpfValidateResponse> execute(String cpf) {
        return webclient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/validator")
                        .queryParam("token", token)
                        .queryParam("value", cpf)
                        .queryParam("type", "cpf")
                        .build())
                .retrieve()
                .bodyToMono(CpfValidateResponse.class)
                .doOnError(ex -> {
                    log.error("erro ao validar cpf na api parceira");
                    throw new ClientException("nao foi possivel validar cpf na api parceira", ex);
                })
                .doOnSuccess(result -> log.info("cpf={} validado na api parceira", result));
    }
}
