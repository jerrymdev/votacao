package com.cooperativa.votacao.infra.integration.cpfvalidate.exception;

public class ClientException extends RuntimeException {
    public ClientException(String message, Throwable error) {
        super(message, error);
    }
}
