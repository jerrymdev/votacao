package com.cooperativa.votacao.infra.integration.cpfvalidate.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CpfValidateResponse {
    Boolean valid;
    String formated;
}
