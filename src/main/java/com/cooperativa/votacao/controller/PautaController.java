package com.cooperativa.votacao.controller;

import com.cooperativa.votacao.controller.DTO.PautaDTO;
import com.cooperativa.votacao.mapper.PautaMapper;
import com.cooperativa.votacao.service.port.BuscarPautaPort;
import com.cooperativa.votacao.service.port.CriarPautaPort;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/pauta")
public class PautaController {

    private final CriarPautaPort criarPautaPort;
    private final BuscarPautaPort buscarPautaPort;
    private final PautaMapper pautaMapper;

    public PautaController(CriarPautaPort criarPautaPort, BuscarPautaPort buscarPautaPort, PautaMapper pautaMapper) {
        this.criarPautaPort = criarPautaPort;
        this.buscarPautaPort = buscarPautaPort;
        this.pautaMapper = pautaMapper;
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    private Mono<PautaDTO> criar(@Valid @RequestBody PautaDTO pautaDTO) {
        return criarPautaPort.execute(pautaMapper.toModel(pautaDTO))
                .map(associadoModel -> pautaMapper.toDTO(associadoModel));
    }

    @GetMapping("/{pautaId}")
    @ResponseStatus(HttpStatus.OK)
    private Mono<PautaDTO> buscar(@PathVariable Long pautaId) {
        return buscarPautaPort.execute(pautaId)
                .map(pautaMapper::toDTO);
    }
}
