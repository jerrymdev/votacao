package com.cooperativa.votacao.controller;

import com.cooperativa.votacao.controller.DTO.VotoDTO;
import com.cooperativa.votacao.infra.persistence.entity.VotoEntity;
import com.cooperativa.votacao.infra.persistence.gateway.impl.BuscarVotosResource;
import com.cooperativa.votacao.mapper.VotoMapper;
import com.cooperativa.votacao.service.port.RegistrarVotoPort;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/voto")
public class VotoController {

    private final RegistrarVotoPort registrarVotoPort;
    private final VotoMapper votoMapper;

    public VotoController(RegistrarVotoPort registrarVotoPort, VotoMapper votoMapper) {
        this.registrarVotoPort = registrarVotoPort;
        this.votoMapper = votoMapper;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    private Mono<VotoDTO> create(@Valid @RequestBody VotoDTO votoDTO) {
        return registrarVotoPort.execute(votoMapper.toModel(votoDTO))
                .map(votoMapper::toDTO);
    }
}
