package com.cooperativa.votacao.controller.exception;

import com.cooperativa.votacao.service.exception.AbrirSessaoException;
import com.cooperativa.votacao.service.exception.BuscarPautaException;
import com.cooperativa.votacao.service.exception.BuscarSessaoException;
import com.cooperativa.votacao.service.exception.CriarPautaException;
import com.cooperativa.votacao.service.exception.RegistrarVotoException;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {

    @ExceptionHandler(WebExchangeBindException.class)
    ResponseEntity<ApiErrorMessage> handleException(WebExchangeBindException e) {
        var errors = e.getBindingResult()
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

        ApiErrorMessage errorResponse = ApiErrorMessage.builder()
                .message("payload invalido")
                .errors(errors)
                .build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler({
            CriarPautaException.class,
            AbrirSessaoException.class,
            RegistrarVotoException.class,
            BuscarSessaoException.class,
            BuscarPautaException.class
    })
    ResponseEntity<ApiErrorMessage> pautaError(Exception ex) {
        log.debug("handling exception::" + ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(buildResponseError(ex.getMessage()));
    }

    @ExceptionHandler({Exception.class})
    ResponseEntity<ApiErrorMessage> defaulException(Exception ex) {
        log.debug("handling exception::" + ex);
        String message = ex.getMessage();

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(buildResponseError(message));

    }


    private ApiErrorMessage buildResponseError(String message) {
        return ApiErrorMessage
                .builder()
                .message(message)
                .build();
    }
}
