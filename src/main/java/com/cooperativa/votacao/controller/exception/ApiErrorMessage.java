package com.cooperativa.votacao.controller.exception;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApiErrorMessage {
    private String message;

    private List<String> errors;

}
