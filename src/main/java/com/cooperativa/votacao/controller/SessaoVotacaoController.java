package com.cooperativa.votacao.controller;

import com.cooperativa.votacao.controller.DTO.AbrirSessaoDTO;
import com.cooperativa.votacao.controller.DTO.SessaoDTO;
import com.cooperativa.votacao.infra.persistence.entity.SessaoVotacaoEntity;
import com.cooperativa.votacao.mapper.SessaoMapper;
import com.cooperativa.votacao.service.port.AbrirSessaoPort;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/sessao")
public class SessaoVotacaoController {

    private final AbrirSessaoPort abrirSessaoPort;
    private final SessaoMapper sessaoMapper;

    public SessaoVotacaoController(AbrirSessaoPort abrirSessaoPort, SessaoMapper sessaoMapper) {
        this.abrirSessaoPort = abrirSessaoPort;
        this.sessaoMapper = sessaoMapper;
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    private Mono<SessaoDTO> create(@Valid @RequestBody AbrirSessaoDTO abrirSessaoDTO) {
        return abrirSessaoPort.execute(abrirSessaoDTO.getIdPauta(), abrirSessaoDTO.getQtyMinutosSessao())
                .map(sessaoMapper::toDTO);
    }
}
