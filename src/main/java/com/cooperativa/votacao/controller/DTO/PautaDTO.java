package com.cooperativa.votacao.controller.DTO;

import jakarta.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PautaDTO {
    public Long id;

    @NotBlank(message = "infome a descricao da pauta")
    public String descricao;

    public LocalDateTime createdAt;

    public Long qtdMinVotos = 1L;

    public Boolean aprovada;

}
