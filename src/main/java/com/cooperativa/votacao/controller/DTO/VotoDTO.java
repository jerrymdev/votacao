package com.cooperativa.votacao.controller.DTO;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VotoDTO {
    public Long id;

    public Long sessaoId;

    @Size(min = 11, max = 11, message = "cpf com formato invalido")
    public String cpf;

    public Voto voto;

    public enum Voto {
        SIM("SIM"), NAO("NAO");

        private final String voto;

        Voto(String voto) {
            this.voto = voto;
        }
    }

}
