package com.cooperativa.votacao.controller.DTO;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AbrirSessaoDTO {

    @NotNull(message = "id da sessao nao informado")
    public Long idPauta;

    public Integer qtyMinutosSessao = 1;

}
