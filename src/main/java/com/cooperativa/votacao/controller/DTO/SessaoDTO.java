package com.cooperativa.votacao.controller.DTO;

import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SessaoDTO {
    public Long id;

    @NotNull(message = "informe uma sessao")
    public Long pautaId;

    public LocalDateTime createdAt;

    public Integer qtMinutos = 1;

    public Boolean ativa;
}
