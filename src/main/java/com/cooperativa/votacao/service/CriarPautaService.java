package com.cooperativa.votacao.service;

import com.cooperativa.votacao.infra.persistence.gateway.CriarPautaGateway;
import com.cooperativa.votacao.mapper.PautaMapper;
import com.cooperativa.votacao.model.PautaModel;
import com.cooperativa.votacao.service.exception.AbrirSessaoException;
import com.cooperativa.votacao.service.port.CriarPautaPort;
import java.util.Date;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class CriarPautaService implements CriarPautaPort {
    private final CriarPautaGateway criarPautaGateway;
    private final PautaMapper mapper;

    public CriarPautaService(CriarPautaGateway criarPautaGateway, PautaMapper mapper) {
        this.criarPautaGateway = criarPautaGateway;
        this.mapper = mapper;
    }

    @Override
    public Mono<PautaModel> execute(PautaModel pautaModel) {
        return criarPautaGateway.execute(mapper.toEntity(pautaModel))
                .map(associadoEntity -> mapper.toModel(associadoEntity))
                .onErrorMap(error -> {
                    throw new AbrirSessaoException("erro ao criar sessao", error);
                });
    }
}
