package com.cooperativa.votacao.service;

import com.cooperativa.votacao.infra.persistence.entity.SessaoVotacaoEntity;
import com.cooperativa.votacao.infra.persistence.gateway.AbrirSessaoGateway;
import com.cooperativa.votacao.mapper.SessaoMapper;
import com.cooperativa.votacao.model.SessaoModel;
import com.cooperativa.votacao.service.exception.AbrirSessaoException;
import com.cooperativa.votacao.service.port.AbrirSessaoPort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class AbrirSessaoService implements AbrirSessaoPort {
    private final AbrirSessaoGateway abrirSessaoGateway;
    private final SessaoMapper mapper;

    public AbrirSessaoService(AbrirSessaoGateway abrirSessaoGateway, SessaoMapper mapper) {
        this.abrirSessaoGateway = abrirSessaoGateway;
        this.mapper = mapper;
    }

    @Override
    public Mono<SessaoModel> execute(Long pautaId, Integer qtMinutosSessao) {
        return abrirSessaoGateway.execute(pautaId, qtMinutosSessao)
                .map(sessaoVotacaoEntity -> mapper.toModel(sessaoVotacaoEntity))
                .onErrorMap(error -> {
                    log.error("erro ao abrir sessao para a pautaId={}", pautaId, error);
                    throw new AbrirSessaoException("erro ao abrir sessao de votacao", error);
                });
    }
}
