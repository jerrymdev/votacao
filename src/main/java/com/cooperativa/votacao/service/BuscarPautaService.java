package com.cooperativa.votacao.service;

import com.cooperativa.votacao.infra.persistence.entity.VotoEntity;
import com.cooperativa.votacao.infra.persistence.gateway.BuscarPautaGateway;
import com.cooperativa.votacao.infra.persistence.gateway.BuscarVotosGateway;
import com.cooperativa.votacao.mapper.PautaMapper;
import com.cooperativa.votacao.mapper.VotoMapper;
import com.cooperativa.votacao.model.PautaModel;
import com.cooperativa.votacao.service.exception.AbrirSessaoException;
import com.cooperativa.votacao.service.exception.BuscarPautaException;
import com.cooperativa.votacao.service.port.BuscarPautaPort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class BuscarPautaService implements BuscarPautaPort {
    private final BuscarPautaGateway buscarPautaGateway;
    private final BuscarVotosGateway buscarVotosGateway;

    private final PautaMapper pautaMapper;
    private final VotoMapper votoMapper;

    public BuscarPautaService(BuscarPautaGateway buscarPautaGateway, BuscarVotosGateway buscarVotosGateway, PautaMapper pautaMapper, VotoMapper votoMapper) {
        this.buscarPautaGateway = buscarPautaGateway;
        this.buscarVotosGateway = buscarVotosGateway;
        this.pautaMapper = pautaMapper;
        this.votoMapper = votoMapper;
    }


    @Override
    public Mono<PautaModel> execute(Long pautaId) {
        return buscarVotosGateway.execute(pautaId)
                .filter(voto -> voto.getVoto() == VotoEntity.Voto.SIM)
                .map(filtrado -> votoMapper.toModel(filtrado))
                .collectList()
                .flatMap(votos -> buscarPautaGateway.execute(pautaId)
                        .map(pautaEntity -> {
                            pautaEntity.setAprovada(votos.size() >= pautaEntity.getQtdMinVotos());
                            return pautaMapper.toModel(pautaEntity);
                        }))
                .onErrorMap(error -> {
                    throw new BuscarPautaException("problemas ao buscar pauta", error);
                });
    }
}
