package com.cooperativa.votacao.service.exception;

public class CriarPautaException extends RuntimeException {
    public CriarPautaException(String message, Throwable error) {
        super(message, error);
    }
}
