package com.cooperativa.votacao.service.exception;

public class RegistrarVotoException extends RuntimeException {
    public RegistrarVotoException(String message, Throwable error) {
        super(message, error);
    }
}
