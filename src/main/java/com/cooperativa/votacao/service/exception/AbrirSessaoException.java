package com.cooperativa.votacao.service.exception;

public class AbrirSessaoException extends RuntimeException {
    public AbrirSessaoException(String message, Throwable error) {
        super(message, error);
    }
}
