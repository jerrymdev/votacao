package com.cooperativa.votacao.service.exception;

public class BuscarSessaoException extends RuntimeException {
    public BuscarSessaoException(String message, Throwable error) {
        super(message, error);
    }
}
