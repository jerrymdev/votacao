package com.cooperativa.votacao.service.exception;

public class BuscarPautaException extends RuntimeException {
    public BuscarPautaException(String message, Throwable error) {
        super(message, error);
    }
}
