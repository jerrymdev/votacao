package com.cooperativa.votacao.service;

import com.cooperativa.votacao.infra.integration.cpfvalidate.gateway.ValidarCPFGateway;
import com.cooperativa.votacao.infra.persistence.gateway.BuscarSessaoGateway;
import com.cooperativa.votacao.infra.persistence.gateway.RegistrarVotoGateway;
import com.cooperativa.votacao.mapper.VotoMapper;
import com.cooperativa.votacao.model.VotoModel;
import com.cooperativa.votacao.service.exception.RegistrarVotoException;
import com.cooperativa.votacao.service.port.RegistrarVotoPort;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class RegistrarVotoService implements RegistrarVotoPort {

    private final RegistrarVotoGateway registrarVotoGateway;
    private final BuscarSessaoGateway buscarSessaoGateway;
    private final ValidarCPFGateway validarCPFGateway;

    private final VotoMapper mapper;

    public RegistrarVotoService(RegistrarVotoGateway registrarVotoGateway, BuscarSessaoGateway buscarSessaoGateway, ValidarCPFGateway validarCPFGateway, VotoMapper mapper) {
        this.registrarVotoGateway = registrarVotoGateway;
        this.buscarSessaoGateway = buscarSessaoGateway;
        this.validarCPFGateway = validarCPFGateway;
        this.mapper = mapper;
    }

    @Override
    public Mono<VotoModel> execute(VotoModel votoModel) {
        return validarCPFGateway.execute(votoModel.getCpf())
                .flatMap(cpfResult -> {
                    if (cpfResult.getValid()) {
                        return buscarSessaoGateway.execute(votoModel.getSessaoId())
                                .flatMap(sessao -> {
                                    boolean sessaoExpirada = isExpired(sessao.getCreatedAt(), sessao.getQtMinutos());
                                    if (sessaoExpirada) {
                                        return Mono.error(new RuntimeException("Sessao expirada."));
                                    } else {
                                        return registrarVotoGateway.execute(mapper.toEntity(votoModel))
                                                .map(voto -> mapper.toModel(voto));
                                    }
                                });
                    } else {
                        log.error("cpf={} invalido para votar", votoModel.getCpf());
                        return Mono.error(new RuntimeException("CPF invalido"));
                    }
                })
                .onErrorMap(error -> {
                    log.error("erro ao registrar voto={}", votoModel, error);
                    throw new RegistrarVotoException("erro ao Registrar voto", error);
                });
    }

    private boolean isExpired(LocalDateTime inicioSessao, Integer sessaoTempoVotacao) {
        LocalDateTime horaAtual = LocalDateTime.now();
        long tempoSessao = ChronoUnit.MINUTES.between(inicioSessao, horaAtual);
        log.info("sessao aberta a {} minutos", tempoSessao);
        if (tempoSessao > sessaoTempoVotacao) {
            return true;
        }
        return false;
    }

}
