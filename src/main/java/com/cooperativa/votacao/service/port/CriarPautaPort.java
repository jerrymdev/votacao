package com.cooperativa.votacao.service.port;

import com.cooperativa.votacao.model.PautaModel;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface CriarPautaPort {
    Mono<PautaModel> execute(PautaModel pautaModel);
}
