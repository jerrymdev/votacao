package com.cooperativa.votacao.service.port;

import com.cooperativa.votacao.model.VotoModel;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface BuscarVotosPort {
    Flux<VotoModel> execute(Long sessaoId);
}
