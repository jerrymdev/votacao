package com.cooperativa.votacao.service.port;

import com.cooperativa.votacao.model.VotoModel;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface RegistrarVotoPort {
    Mono<VotoModel> execute(VotoModel votoModel);
}
