package com.cooperativa.votacao.service.port;

import com.cooperativa.votacao.model.SessaoModel;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface AbrirSessaoPort {
    Mono<SessaoModel> execute(Long pautaId, Integer qtMinutosSessao);
}
