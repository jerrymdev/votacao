package com.cooperativa.votacao.mapper;

import com.cooperativa.votacao.controller.DTO.SessaoDTO;
import com.cooperativa.votacao.infra.persistence.entity.SessaoVotacaoEntity;
import com.cooperativa.votacao.model.PautaModel;
import com.cooperativa.votacao.model.SessaoModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SessaoMapper {
    SessaoVotacaoEntity toEntity(SessaoModel sessaoModel);
    SessaoModel toModel(SessaoVotacaoEntity sessaoVotacaoEntity);
    SessaoModel toModel(SessaoDTO sessaoDTO);
    SessaoDTO toDTO(SessaoModel sessaoModel);
}
