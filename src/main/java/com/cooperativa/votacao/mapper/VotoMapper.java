package com.cooperativa.votacao.mapper;

import com.cooperativa.votacao.controller.DTO.VotoDTO;
import com.cooperativa.votacao.infra.persistence.entity.VotoEntity;
import com.cooperativa.votacao.model.VotoModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VotoMapper {
    VotoEntity toEntity(VotoModel votoModel);
    VotoModel toModel(VotoEntity votoEntity);

    VotoModel toModel(VotoDTO votoDTO);

    VotoDTO toDTO(VotoModel votoModel);
}
