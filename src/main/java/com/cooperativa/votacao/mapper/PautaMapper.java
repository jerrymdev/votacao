package com.cooperativa.votacao.mapper;

import com.cooperativa.votacao.controller.DTO.PautaDTO;
import com.cooperativa.votacao.infra.persistence.entity.PautaEntity;
import com.cooperativa.votacao.model.PautaModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PautaMapper {
    PautaEntity toEntity(PautaModel pautaModel);
    PautaModel toModel(PautaEntity associadoModel);
    PautaModel toModel(PautaDTO pautaDTO);
    PautaDTO toDTO(PautaModel pautaModel);
}
