package com.cooperativa.votacao.model;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SessaoModel {

    public Long id;

    public Long pautaId;

    public LocalDateTime createdAt;

    public Integer qtMinutos;

    public Boolean ativa;

}
