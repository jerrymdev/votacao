package com.cooperativa.votacao.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VotoModel {

    public Long id;

    public Long sessaoId;

    public String cpf;

    public Voto voto;

    public enum Voto {
        SIM("SIM"), NAO("NAO");

        private final String voto;

        Voto(String voto) {
            this.voto = voto;
        }
    }
}
