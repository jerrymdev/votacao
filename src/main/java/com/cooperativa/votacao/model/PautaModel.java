package com.cooperativa.votacao.model;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PautaModel {
    public Long id;

    public String descricao;

    public LocalDateTime createdAt;

    public Long qtdMinVotos;


    public Boolean aprovada;

}
