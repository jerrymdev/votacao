package com.cooperativa.votacao.dockerconfig;

import io.r2dbc.spi.ConnectionFactoryOptions;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.PostgreSQLR2DBCDatabaseContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
public class PostgresContainer implements BeforeAllCallback, AfterAllCallback {

    private PostgreSQLContainer<?> postgres;

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        postgres = new PostgreSQLContainer<>("postgres:11")
                .withDatabaseName("assembleia")
                .withUsername("test")
                .withPassword("test")
                .withExposedPorts(5432)
                .withInitScript("schema.sql");

        postgres.start();
        String jdbcUrl = String.format("r2dbc:pool:postgres://test:test@localhost:%d/assembleia", postgres.getFirstMappedPort());

        System.setProperty("jdbcUrl", jdbcUrl);

    }

    @Override
    public void afterAll(ExtensionContext context) throws Exception {

    }
}
