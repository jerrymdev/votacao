package com.cooperativa.votacao.controller;

import com.cooperativa.votacao.bean.BeanManager;
import com.cooperativa.votacao.controller.DTO.PautaDTO;
import com.cooperativa.votacao.controller.DTO.VotoDTO;
import com.cooperativa.votacao.dockerconfig.PostgresContainer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(port = 0)
@Import(value = BeanManager.class)
@ExtendWith(value = PostgresContainer.class)
@DirtiesContext
class VotoControllerTest {

	@Autowired
	private WebTestClient webClient;

	@Test
	@DisplayName("dado que ja existe uma pauta e uma sessao com id 1, deve registrar um voto")
	void shouldGetPong()  {
		var voto = VotoDTO.builder()
						.voto(VotoDTO.Voto.SIM)
						.cpf("00488563254")
						.sessaoId(1L)
						.build();

		webClient.post().uri("/voto")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(voto), VotoDTO.class)
				.exchange()
				.expectStatus().isCreated()
				.expectBody()
				.jsonPath("$.cpf").isEqualTo(voto.getCpf());
	}

}
