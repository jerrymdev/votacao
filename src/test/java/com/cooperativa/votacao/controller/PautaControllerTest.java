package com.cooperativa.votacao.controller;

import com.cooperativa.votacao.bean.BeanManager;
import com.cooperativa.votacao.controller.DTO.PautaDTO;
import com.cooperativa.votacao.dockerconfig.PostgresContainer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Import(value = BeanManager.class)
@ExtendWith(value = PostgresContainer.class)
@DirtiesContext
class PautaControllerTest {

	@Autowired
	private WebTestClient webClient;

	@Test
	@DisplayName("deve buscar uma pauta")
	void shouldBeGetPauta() {
		webClient.get().uri("/pauta/{pautaId}", 1)
				.exchange()
				.expectStatus().isOk();
	}

	@Test
	@DisplayName("deve cadastrar uma pauta")
	void shouldCreatePauta() throws Exception {
		var pauta = PautaDTO.builder()
						.descricao("pauta test")
						.qtdMinVotos(50L)
						.build();

		webClient.post().uri("/pauta")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(pauta), PautaDTO.class)
				.exchange()
				.expectStatus().isCreated()
				.expectBody()
				.jsonPath("$.descricao").isEqualTo(pauta.getDescricao());
	}

}
