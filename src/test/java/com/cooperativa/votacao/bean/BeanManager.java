package com.cooperativa.votacao.bean;

import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.ConnectionFactoryOptions;
import org.springframework.boot.r2dbc.ConnectionFactoryBuilder;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@TestConfiguration
public class BeanManager {
    @Primary
    @Bean
    public ConnectionFactory connectionFactory() {
        String jdbcUrl = System.getProperty("jdbcUrl");
        return ConnectionFactoryBuilder.withUrl(jdbcUrl).build();
    }
}
