CREATE TABLE IF NOT EXISTS pauta (
                                     id SERIAL constraint id_pk primary key,
                                     descricao  text not null,
                                     created_at timestamp not null,
                                     qtd_min_votos numeric,
                                     aprovada   bool
);

CREATE TABLE IF NOT EXISTS sessao_votacao
(
    id  serial
    constraint sessao_votacao_pk
    primary key
    constraint sessao_votacao_pauta_id_fk
    references pauta,
    fk_pauta    serial,
    created_at  timestamp   not null,
    qt_minutos integer,
    ativa       bool
);

create table votos
(
    id        serial
        constraint votos_pk
            primary key,
    sessao_id serial
        constraint sessao_fk
            references sessao_votacao,
    cpf       text not null,
    voto char(3),
    unique (sessao_id, cpf)
);

insert into pauta (descricao, created_at, qtd_min_votos, aprovada) values ('pauta test', now(), 10, null);

insert into sessao_votacao (fk_pauta, created_at, qt_minutos, ativa) VALUES (1, now(), 10, true);