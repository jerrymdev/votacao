CREATE TABLE IF NOT EXISTS pauta (
                                     id SERIAL constraint id_pk primary key,
                                     descricao  text not null,
                                     created_at timestamp not null,
                                     qtd_min_votos numeric,
                                     aprovada   bool
);

CREATE TABLE IF NOT EXISTS sessao_votacao
(
    id  serial
    constraint sessao_votacao_pk
    primary key
    constraint sessao_votacao_pauta_id_fk
    references pauta,
    fk_pauta    serial,
    created_at  timestamp   not null,
    qt_minutos integer,
    ativa       bool
);

create table votos
(
    id        serial
        constraint votos_pk
            primary key,
    sessao_id serial
        constraint sessao_fk
            references sessao_votacao,
    cpf       text not null,
    voto char(3),
    unique (sessao_id, cpf)
);